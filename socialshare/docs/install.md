Install
=======

Install in edit mode:

	pip install -e git+https://guillaumepiot@bitbucket.org/guillaumepiot/cotidia-social-share.git#egg=socialshare
	
Install in read only:

	pip install git+https://guillaumepiot@bitbucket.org/guillaumepiot/cotidia-social-share.git